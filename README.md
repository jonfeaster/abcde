# abcde Custom Configurations

An abcde CD encoder configuration

View the live version at [https://gitlab.com/jonfeaster/abcde](https://gitlab.com/jonfeaster/abcde)

The vast majority of these configurations assume that the user desires the mp3 format as it is the most widely supported format. Unfortunately this format is proprietary. A great alternative is the free lossless [FLAC](https://xiph.org/flac/) audio coding format. Of course, a configuration file for that is included. Live free!

## Installation

1. Install abcde: [abcde](https://abcde.einval.com/) or install in Ubuntu (`sudo apt install abcde`)
2. For MP3, ensure all required codecs etc. are installed (e.g. Lame: `sudo apt install lame`, id3v2: `sudo apt install id3v2`). For FLAC, ensure FLAC is installed: `sudo apt install flac`.
3. Copy the desired abcde-*.conf file (by output format) to your home directory (e.g. `~/`) and make any desired changes in it
4. Rename the copied abcde-*.conf to .abcde.conf to make it a hidden file recognized by abcde
5. If you are having touble with a disc you can try changing the cd ripping program via `CDROMREADERSYNTAX` (e.g. `CDROMREADERSYNTAX=cdda2wav`). ([abcde Very Slow For Corrections](https://ubuntuforums.org/archive/index.php/t-1568620.html))

## Run

- Run a default rip (44.1kHz, 128kbps) in the terminal: `abcde`
- Run an mp3 rip at 44.1kHz, 192kbps in the terminal: `abcde -o mp3:"-b 192" -G`

## Key Configuration Variables

- CDDBMETHOD
- CDROM
- CDROMREADERSYNTAX
- OUTPUTDIR
- WAVOUTPUTDIR
- OUTPUTTYPE
- OUTPUTFORMAT

## Etc

- Add a bash alias (e.g. ~/.bash_aliases)
  - `alias ripcd='abcde'`
  - `alias ripcd='abcde -o mp3:"-b 192" -G'`
  - `alias ripcd-flac='abcde -c $HOME/.abcde-flac.conf -G'`
  - `alias ripcd-mp3='abcde -c $HOME/.abcde-mp3.conf -o mp3:"-b 192" -G'`
  - `alias ripcd-slow='cd ~/ && abcde -S 1'`

## Resources

- [abcde](https://abcde.einval.com/)
- [abcde man page](https://www.mankier.com/1/abcde)
- [Andrew's Corner: A collection of abcde.conf files](http://andrews-corner.org/linux/abcde/index.html)
- [CDRipping](https://help.ubuntu.com/community/CDRipping)
- [[HOWTO] abcde](https://ubuntuforums.org/showthread.php?t=109429)
- [How to Rip CDs From the Linux Command Line](https://linuxconfig.org/how-to-rip-cds-from-the-linux-command-line)
- [Rip CD's with abcde](https://notes.enovision.net/linux/rip-cds-with-abcde)
- [How to Easily Rip a CD with Abcde](https://www.maketecheasier.com/rip-cd-with-abcde/)
- [Use abcde to produce high quality flac and mp3 output with album art under Xenial?](https://askubuntu.com/questions/788327/use-abcde-to-produce-high-quality-flac-and-mp3-output-with-album-art-under-xenia)
